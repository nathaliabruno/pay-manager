import React from 'react'
import Link from 'next/link'
import Table from '../components/Table'
import HeaderPage from '../components/Header'
import 'isomorphic-fetch'

export default class TableList extends React.Component {
  static async getInitialProps () {
    const res = await fetch('https://demo0459370.mockable.io/paymanager')
    const json = await res.json()
    return {tableList: json.tables}
  }

  render () {
    const tables = this.props.tableList

    return (
      <main>
        <HeaderPage />
        <h3>Selecione a mesa</h3>
        <div className="table-list">
          {tables.map( (table, index) => (
            <Table
              key={index}
              name={table.name}
              id={table.id}
            />
          ))}
        </div>
        <style jsx global>{`
          * {
            padding: 0;
            border: none;
            margin: 0;
          }
          body {
            font-family: 'Open Sans', Arial, sans-serif;
          }
          main {
            position: absolute;
            height: 100%;
            width: 100%;
          }
        `}</style>

        <style jsx>{`
          .table-list {
            display: flex;
            flex-wrap: wrap;
            justify-content: center;
            align-self: center;
            max-width: 80%;
            margin: auto;
          }
          h3 {
            text-align: center;
            margin: 10% 0;
          }

        `}</style>
      </main>
    )
  }
}

