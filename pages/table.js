import React from 'react'
import Link from 'next/link'
import ListItem from '../components/ListItem'
import HeaderPage from '../components/Header'
import InputText from '../components/InputText'
import Button from '../components/Button'
import 'isomorphic-fetch'

export default class TableDetail extends React.Component {
	static async getInitialProps () {
		const res = await fetch('https://demo0459370.mockable.io/paymanager')
		const json = await res.json()
		return {tableList: json.tables}
	}

	constructor (props) {
		super(props);
		this.state = {
			amount_payd: 0
		}
		this.handleClick = this.handleClick.bind(this);
		this.showPayment = this.showPayment.bind(this);

	}

	handleClick() {
		this.setState(amount_payd => ({
			amount_payd: this.state.amount_payd + (parseFloat(document.getElementById('insert-payment').value) || 0)
		}));
  	}

	showPayment() {
		document.querySelector('.payment-proccess').classList.add('active');
	}

	render () {
		const tableId = window.location.search.replace("?", "");

		const tableData = this.props.tableList[tableId];
		const productList = tableData.products
		let total = 0;

		productList.map( (product, index) => (
			total = total + parseFloat(product.price)
		));

		return (

			<main>
				<HeaderPage/>
				<div className="table-container">
					<h1 className="table-name">{this.props.tableList[tableId].name}</h1>
					<h3>Produtos:</h3>
					<ul className="list-product">
						{productList.map( (product, index) => (
							<ListItem
								key={index}
								name={product.productName}
								value={product.price}
							/>
						))}
					</ul>
					<Button
						text="Iniciar Pagamento"
						event={this.showPayment}
						bgColor="#3eb75b"
					/>
				</div>

				<section className="payment-proccess">
					<ul className="payment-details">
						<ListItem
							name="Total"
							value={total.toFixed(2)}
						/>
						<ListItem
							name="Valor Pago"
							value={this.state.amount_payd.toFixed(2)}
						/>
						<ListItem
							name="Falta Pagar"
							value={parseFloat(total - this.state.amount_payd).toFixed(2)}
						/>
					</ul>
					<InputText
						id="insert-payment"
						placeholder="0.00"
						label="Insira o valor pago"
						type="number"
						maxValue={parseFloat(total - this.state.amount_payd).toFixed(2)}
					/>
					{
					(total > this.state.amount_payd) ?
					<Button
						text="Inserir"
						event={this.handleClick}
						bgColor="#3eb75b"
					/>
					:
					<Button
						text="Finalizar pagamento"
						url="/"
					/>
					}
				</section>
				<style jsx global>{`
					* {
						padding: 0;
						border: none;
						margin: 0;
						box-sizing: border-box;
					}
					body {
						font-family: 'Open Sans', Arial, sans-serif;
					}
					main {
						position: absolute;
						height: 100%;
						width: 100%;
					}
				`}</style>
				<style jsx>{`
					.table-name {
						text-align: center;
						font-size: 1.5rem;
					}
					.table-container {
						width: 60%;
						margin: auto;
						height: calc(100% - 5rem);
						padding-top: 2rem;
					}
					h3 {
						margin-top: 2rem;
						text-align: center;
					}
					.list-product {
						padding: 2rem 0;
					}
					.payment-proccess {
						position: absolute;
						left: -100%;
						top: 5rem;
						width: 100%;
					}
					.payment-proccess.active {
						left: 0;
						padding: 0 20%;
						background-color: #fff;
						top: 25%;
						height: 70%;
					}
				`}</style>
			</main>
		)
	}
}

