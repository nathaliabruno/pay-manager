import Link from 'next/link'
import { string, number } from 'prop-types'

const InputText = ({label, placeholder, type, id, maxValue}) => (
  <div className="input-group">

    <label htmlFor={id}>
      {label}
    </label>
    <input
      type={type}
      id={id}
      placeholder={placeholder}
      max={maxValue}
      step="0.01"
      />

  <style jsx>{`
    label {
      font-size: 0.7rem;
      display: block;
      margin-top: 2rem;
    }
    label,
    input {
      width: 100%;
    }

    input {
      appearance: textfield;
      border: 1px solid #dedede;
      padding: 0.5rem;
      font-size: 1.3rem;
      border-radius: 3px;
      transition: border-left-width 0.3s ease;
    }
    input:focus {
      outline: none;
      border-left-width: 4px;
    }
  `}</style>
  </div>
)

InputText.PropTypes = {
  id: string,
  label: string,
  type: string,
  placeholder: string,
  maxValue: number
};

InputText.defaultProps = {
  id: "",
  label: "",
  type: "text",
  placeholder: "",
  maxValue: null
}

export default InputText
