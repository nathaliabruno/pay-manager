import { string } from 'prop-types';
import Link from 'next/link';

const HeaderPage = () => (
	<header className="top-bar">
		<img src="../static/logo-stone-02.svg" alt="Stone Pagamentos" className="logo-stone"/>
		<Link href="/">
			<h2 className="branding">Pay Manager</h2>
		</Link>

		<style jsx>{`

			.top-bar {
				height: 5rem;
				box-shadow: 1px 1px 5px rgba(0,0,0,0.2);
				display: flex;
				width: 100%;
				justify-content: center;
				align-items: center;
			}
			.logo-stone {
				max-height: 60%;
				width: auto;
				margin-right: 2%;
			}
			.branding {
				font-weight: 300;
				text-transform: uppercase;
				font-size: 1rem;
				cursor: pointer;
			}

		`}</style>
	</header>
)

export default HeaderPage
