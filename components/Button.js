import Link from 'next/link'
import { string, any } from 'prop-types'

const Button = ({url, text, color, bgColor, event}) => (
  url ?
  <Link prefetch href={url}>
    <button
      className="button"
      style={{
        color: color,
        backgroundColor: bgColor
    }}>

      {text}

    <style jsx>{`
      button {
        border: none;
        font-size: 0.8rem;
        width: 100%;
        padding: 0.8rem;
        text-align: center;
        margin-top: 2rem;
        border-radius: 3px;
        transition: opacity 0.3s ease;
        font-weight: bold;
      }
      button:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    `}</style>
    </button>
  </Link>
  :
    <button
      className="button"
      onClick={event}
      style={{
        color: color,
        backgroundColor: bgColor
    }}>

      {text}

    <style jsx>{`
      button {
        border: none;
        font-size: 0.8rem;
        width: 100%;
        padding: 0.8rem;
        text-align: center;
        margin-top: 2rem;
        border-radius: 3px;
        transition: opacity 0.3s ease;
        font-weight: bold;
      }
      button:hover {
        cursor: pointer;
        opacity: 0.9;
      }
    `}</style>
    </button>
)

Button.PropTypes = {
  url: string,
  text: string,
  color: string,
  bgColor: string,
  event: any
};

Button.defaultProps = {
  url: null,
  text: "Ok",
  color: "#000",
  bgColor: "#CCC",
}

export default Button
