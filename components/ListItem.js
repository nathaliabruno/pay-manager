import Link from 'next/link'
import { string, number } from 'prop-types'

const ListItem = ({name, value}) => (
  <li className="list-item">

    <div className="name">
      {name}
    </div>
    <div className="value">
      {value}
    </div>

  <style jsx>{`
    li {
      border: none;
      font-size: 0.8rem;
      display: flex;
      justify-content: space-between;
      align-items: center;
      border-bottom: 1px solid #ddd;
      line-height: 2
    }
  `}</style>
  </li>
)

ListItem.PropTypes = {
  key: string,
  value: number,
};

ListItem.defaultProps = {
  key: "Produto X",
  value: "11.10",
}

export default ListItem
