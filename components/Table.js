import Link from 'next/link'
import React from 'react'
import { string, number } from 'prop-types'

class Table extends React.Component {

  render () {
    const {
      id,
      name
    } = this.props

    return(
      <Link prefetch href={`/table?${id}`}>
        <section
          className="table">
          <img src="/static/table-image.svg" alt="table"/>
          <h4>
            {name}
          </h4>

        <style jsx>{`
          section {
            border: 1px solid rgba(0,0,0,0.3)
            font-size: 0.8rem;
            height: 30%;
            min-height: 5rem;
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            flex-basis: 40%;
            transition: background-color 0.3s ease;
          }
          section:hover {
            cursor: pointer;
            background-color: rgba(0,0,0,0.05);
          }
          h4 {
            margin-top: 5%;
          }
        `}</style>
        </section>
      </Link>
    )
  }
}

Table.PropTypes = {
  id: number,
  name: string,
};

Table.defaultProps = {
  id: 0,
  name: "table",
}

export default Table
